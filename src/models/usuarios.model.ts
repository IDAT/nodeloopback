import { Entity, model, property } from '@loopback/repository';

@model(
  //{ name: 'tbusuarios' }
  {
    settings: {
      mysql: {
        schema: '8fWeJ95SuZ',
        table: 'tbusuarios'
      }
    }
  }
)
export class Usuarios extends Entity {
  @property({
    type: 'string',
    id: true,
    required: true,
  })
  usu_codigo: string;

  @property({
    type: 'string'
  })
  usu_descri: string;

  @property({
    type: 'string'
  })
  usu_email: string;

  @property({
    type: 'number'
  })
  usu_estcod: number;

  @property({
    type: 'string',
  })
  usu_fecreg?: string;

  @property({
    type: 'string',
  })
  usu_imagen?: string;

  @property({
    type: 'string'
  })
  usu_nombre: string;

  @property({
    type: 'string'
  })
  usu_passwd: string;


  constructor(data?: Partial<Usuarios>) {
    super(data);
  }
}

export interface UsuariosRelations {
  // describe navigational properties here
}

export type UsuariosWithRelations = Usuarios & UsuariosRelations;
